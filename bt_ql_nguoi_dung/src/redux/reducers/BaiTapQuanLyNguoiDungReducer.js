import {add_user,remove_user,editUser,updateUser} from "../types/BaiTapNguoiDungType";
import Swal from 'sweetalert2';

const initialState = {
    userList:[
        {id:"1",taiKhoan:"nguyenvana",hoTen:"nguyen van a",matKhau:"123",email:"nguyenvana@gmail.com",sDT:"0123456789",loaiNguoiDung:"khach hang"},
        {id:"2",taiKhoan:"nguyenvana",hoTen:"nguyen van a",matKhau:"123",email:"nguyenvana@gmail.com",sDT:"0123456789",loaiNguoiDung:"khach hang"}
    ],
    error:{taiKhoan:"",hoTen:"",matKhau:"",email:"",sDT:""},
    userEdit:{id:"-1",taiKhoan:"",hoTen:"",matKhau:"",email:"",sDT:"",loaiNguoiDung:""}
}

export default (state=initialState,action)=>{
    switch(action.type)
    {
        case add_user:{
            //console.log("add",action.user);
            
            let errorContent = "";
                let errorNew = {...state.error,taiKhoan:"",hoTen:"",matKhau:"",email:"",sDT:""};
                if(action.user.taiKhoan.trim()===''){errorNew.taiKhoan="<p class="+"text-left"+">Tài khoản không được để trống !</p>"}
                if(action.user.hoTen.trim()===''){errorNew.hoTen="<p class="+"text-left"+">Họ tên không được để trống !</p>"}
                if(action.user.matKhau.trim()===''){errorNew.matKhau="<p class="+"text-left"+">Mật khẩu không được để trống !</p>"}
                if(action.user.email.trim()===''){errorNew.email="<p class="+"text-left"+">Email không được để trống !</p>"}
                if(action.user.sDT.trim()===''){errorNew.sDT="<p class="+"text-left"+">Số điện thoại không được để trống !</p>"}

                for(let key in errorNew)
                {
                    if(errorNew[key]==="")
                    {
                        let userUpdate = [...state.userList];
                        userUpdate.push(action.user);
                        state.userList = userUpdate;           
                        return {...state} 
                    }else{
                        errorContent+= errorNew[key];
                    }
                }
                
                state.error = errorNew;
                Swal.fire({
                    title: 'Error!',
                    html: errorContent,
                    icon: 'error',
                    confirmButtonText: 'OK'
                  })
                return {...state}
                                  
        }
        case remove_user:{
            let listUpdate = [...state.userList];
            listUpdate = listUpdate.filter(user => user.id !==action.userId);
            
            return {...state,userList:listUpdate}
        }
        case editUser:{
            return {...state,userEdit:action.user}
        }
        case updateUser:{
            //console.log(action.user)
            state.userEdit = {...state.userEdit, taiKhoan:action.user.taiKhoan,hoTen:action.user.hoTen,matKhau:action.user.matKhau,email:action.user.email};
            let listUpdate = [...state.userList];
            let index = listUpdate.findIndex(user => user.id === state.userEdit.id);
            if(index !== -1)
            {
                listUpdate[index] = state.userEdit;
            }
            state.userList = listUpdate;
            state.userEdit = {id:"-1",taiKhoan:"",hoTen:"",matKhau:"",email:"",sDT:"",loaiNguoiDung:""}
            return {...state}
        }
        default:
            return {...state}
    }
}