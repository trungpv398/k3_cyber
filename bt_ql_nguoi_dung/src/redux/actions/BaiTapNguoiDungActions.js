import { add_user,remove_user,editUser,updateUser} from "../types/BaiTapNguoiDungType";


export const addNewAction = (user) =>({
    type:add_user,
    user
})
export const deleteAction = (userId) =>({
    type:remove_user,
    userId
})
export const editAction = (user) =>({
    type:editUser,
    user
})

export const updateAction = (user) =>({
    type:updateUser,
    user
})