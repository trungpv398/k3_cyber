import React, { Component } from 'react';
import {connect} from 'react-redux';
import {TitleListUser,Table,ButtonEdit,ButtonDelete,Thead,Tr,Th,Tbody,Td,Content} from './Themes/BTQuanLyNguoiDungTheme'
import { deleteAction,editAction } from '../redux/actions/BaiTapNguoiDungActions'

class DanhSachNguoiDung extends Component {

    renderListUder = ()=>{
        //console.log(this.props.listUser)
        return this.props.listUser.map((user,index)=>{
            return <Tr key={index}>
            <Td>{index+1}</Td>
            <Td>{user.taiKhoan}</Td>
            <Td>{user.hoTen}</Td>
            <Td>{user.matKhau}</Td>
            <Td>{user.email}</Td>
            <Td>{user.sDT}</Td>
            <Td>{user.loaiNguoiDung}</Td>
            <Td>
                <ButtonEdit onClick={()=>{
                    this.props.dispatch(editAction(user))
                }}>Chỉnh sửa</ButtonEdit>
                <ButtonDelete onClick={()=>{
                    this.props.dispatch(deleteAction(user.id))
                }} className="btn btn-danger">Xoá</ButtonDelete>
            </Td>
        </Tr>
        })
    }
    render() {
        return (
            <div >
                <TitleListUser className="col-12" >
                    Danh sách người dùng 
                </TitleListUser>
                <Content className="col-12" >
                    <Table className=" col-12">
                        <Thead>
                            <Tr>
                                <Th>STT</Th>
                                <Th>Tài khoản</Th>
                                <Th>Họ tên</Th>
                                <Th>Mật khẩu</Th>
                                <Th>Email</Th>
                                <Th>Số điện thoại</Th>
                                <Th>Loại người dùng</Th>
                                <Th></Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {this.renderListUder()}                          
                        </Tbody>
                    </Table>
                </Content>
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return{
        listUser: state.BaiTapQuanLyNguoiDungReducer.userList
        
    }
}


export default connect(mapStateToProps)(DanhSachNguoiDung)