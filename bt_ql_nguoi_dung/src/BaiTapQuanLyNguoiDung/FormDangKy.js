import React, { Component } from 'react'
import { addNewAction,updateAction } from '../redux/actions/BaiTapNguoiDungActions'
import {TitleFormRegister,Content,Label,ButtonEdit,ButtonRegister} from './Themes/BTQuanLyNguoiDungTheme'
import { connect } from "react-redux";

class FormDangKy extends Component {
    state = {
        id:'',
        taiKhoan : '',
        hoTen:'',
        matKhau:'',
        sDT:'',
        email:'',
        loaiNguoiDung:'khach hang',
        disabled:true

    }
    render() {
        return (
            <div className="mb-4 mt-4">
                <TitleFormRegister className="col-12">
                    Form đăng ký
                </TitleFormRegister>
                <Content className="col-12 pt-4 pb-4">
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <Label htmlFor="exampleInputEmail1">Tài khoản</Label>
                                <input type="text" name="taiKhoan" value={this.state.taiKhoan}  onChange={(e)=>{this.setState({taiKhoan:e.target.value})}} className="form-control"  aria-describedby="emailHelp"  />  
                                                   
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="form-group">
                                <Label htmlFor="exampleInputEmail1">Họ tên</Label>
                                <input type="text" name="hoTen" value={this.state.hoTen} className="form-control" onChange={(e)=>{this.setState({hoTen:e.target.value})}}  aria-describedby="emailHelp"  />                        
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <Label htmlFor="exampleInputEmail1">Mật khẩu</Label>
                                <input type="password" value={this.state.matKhau} name="matKhau" className="form-control" onChange={(e)=>{this.setState({matKhau:e.target.value})}}  aria-describedby="emailHelp" />                        
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="form-group">
                                <Label htmlFor="exampleInputEmail1">Số điện thoại</Label>
                                <input type="number" value={this.state.sDT} name="sDT" className="form-control" onChange={(e)=>{this.setState({sDT:e.target.value})}} aria-describedby="emailHelp"  />                        
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <Label htmlFor="exampleInputEmail1">Email </Label>
                                <input type="email" value={this.state.email} name="email" className="form-control" onChange={(e)=>{this.setState({email:e.target.value})}} aria-describedby="emailHelp" />                        
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="form-group">
                            <Label htmlFor="exampleInputEmail1">Mã loại người dùng</Label>
                                <select className="form-control" onChange={(e)=>{this.setState({loaiNguoiDung:e.target.value},()=>{console.log(this.state)})}} name="loaiNguoiDung">
                                    <option value="khach hang">khach hang</option>
                                    <option value="khach hang1">khach hang1</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    
                   
                    <ButtonRegister onClick={()=>{
                        let newUser = {...this.state};
                        //console.log(newUser);
                        this.props.dispatch(addNewAction(newUser))
                    }}>Đăng ký</ButtonRegister>
                    {
                        this.state.disabled ? <ButtonEdit disabled style={{cursor:"no-drop"}} onClick={()=>{
                            let user=this.state;
                            this.props.dispatch(updateAction(user))
                        }}>Cập nhật</ButtonEdit>:
                        <ButtonEdit onClick={()=>{
                            let user=this.state;
                            this.setState({
                                id:'',taiKhoan : '',hoTen:'',matKhau:'',sDT:'',email:'',loaiNguoiDung:'khach hang',disabled:true
                            })
                            this.props.dispatch(updateAction(user))
                        }}>Cập nhật</ButtonEdit>
                    }
                   

                </Content>
                
            </div>
        )
    }
    componentDidUpdate(prevProps, prevState){
        if(prevProps.userEdit.id !== this.props.userEdit.id){
            this.setState({
                id:this.props.userEdit.id,
                taiKhoan : this.props.userEdit.taiKhoan,
                hoTen:this.props.userEdit.hoTen,
                matKhau:this.props.userEdit.matKhau,
                sDT:this.props.userEdit.sDT,
                email:this.props.userEdit.email,
                disabled:false
            })
        }
    }
}
const mapStateToProps = state =>{
    return {
        errorList:state.BaiTapQuanLyNguoiDungReducer.error,
        userEdit:state.BaiTapQuanLyNguoiDungReducer.userEdit
    }
}

export default connect(mapStateToProps)(FormDangKy)
