import styled from 'styled-components';
export const Container=styled.div`
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`

export const Title= styled.div`
    background-color: rgba(0, 0, 0, 0.77);
    color: rgb(255, 255, 255);
    padding: 10px;
    border-radius: 5px 5px 0px 0px;
`
export const TitleListUser=styled(Title)`
    font-size:15px;
`
export const TitleFormRegister=styled(Title)`
    font-size:20px;
`
export const Content = styled.div`
    border: 1px solid #000000c4;
`

export const Table=styled.div`
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
    border-collapse: collapse;
`
export const Thead=styled.thead`
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
`
export const Tr = styled.tr`
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
`
export const Th = styled.th`
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
    padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
`
export const Tbody = styled.tbody`
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
`
export const Td = styled.td`
    padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
`

export const Button = styled.div`
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    on   transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
`

export const ButtonEdit=styled(Button)`
    color: #fff;
    background-color: #007bff;
    border-color: #007bff;
    margin:1px;
`
export const ButtonDelete=styled(Button)`
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545;
    margin:1px;
`
export const ButtonRegister=styled(Button)`
    color: #fff;
    background-color: #509e14;
    border-color: #509e14;
    margin:1px;
` 


//form input

export const Label = styled.div`
    display: inline-block;
    margin-bottom: .5rem;
    cursor: default;
`