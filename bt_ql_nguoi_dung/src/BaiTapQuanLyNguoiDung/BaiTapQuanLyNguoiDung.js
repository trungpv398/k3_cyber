import React, { Component } from 'react';
import DanhSachNguoiDung from './DanhSachNguoiDung';
import FormDangKy from './FormDangKy';
import {Container}from './Themes/BTQuanLyNguoiDungTheme';


export default class BaiTapQuanLyNguoiDung extends Component {
    render() {
        
        return (
            <div className="container">
                <FormDangKy/>
                <DanhSachNguoiDung/>
            </div>
        )
    }
}
