import React from 'react'
import { useSelector } from "react-redux";

export default function ThuDo() {
    
    const thuGiay = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThuGiay);
    const thuAo = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThuAo);
    const thuToc = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThukieuToc);
    const thuTuiXach = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThuTuiXach);
    const thuDaychuyen = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThuDayChuyen);
    const thuQuan = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThuQuan);
    const thuNen = useSelector((state)=>state.ThuDoTrucTuyenReducer.ThuNen)

    return (
        <div className="contain">
        <div className="body" />
        <div className="model" />
        <div
          className="hairstyle"
          style={{
            width: 1000,
            height: 1000,
            position: "absolute",
            top: "-75%",
            right: "-57%",
            transform: "scale(0.15)",
            zIndex: 4,
          }}
        ><img src={thuToc.imgSrc_png} /></div>
        <div
          className="necklace"
          style={{
            width: 500,
            height: 1000,
            background: 'url("./images/necklaces/necklace1.png")',
            position: "absolute",
            bottom: "-40%",
            right: "-3.5%",
            transform: "scale(0.5)",
            zIndex: 4,
          }}
        ><img src={thuDaychuyen.imgSrc_png}/></div>
        <div
          className="bikinitop"
          style={{
            width: 500,
            height: 500,
            position: "absolute",
            top: "-9%",
            left: "-5%",
            zIndex: 3,
            transform: "scale(0.5)",
          }}
        >
            <img src={thuAo.imgSrc_png}/>
        </div>
        <div
          className="bikinibottom"
          style={{
            width: 500,
            height: 1000,
            //background: 'url("./images/clothes/botcloth4.png")',
            position: "absolute",
            top: "-30%",
            left: "-5%",
            zIndex: 2,
            transform: "scale(0.5)",
          }}
        ><img src={thuQuan.imgSrc_png}/></div>
        <div
          className="handbag"
          style={{
            width: 500,
            height: 1000,
           
            position: "absolute",
            bottom: "-40%",
            right: "-3.5%",
            transform: "scale(0.5)",
            zIndex: 4,
          }}
        ><img src={thuTuiXach.imgSrc_png}/></div>
        <div
          className="feet"
          style={{
            width: 500,
            height: 1000,              
            position: "absolute",
            bottom: "-37%",
            right: "-3.5%",
            transform: "scale(0.5)",
            zIndex: 1,
          }}
        ><img src={thuGiay.imgSrc_png}/></div>
        <div
          className="background"
          style={{
            //backgroundImage: 'url("./images/background/background2.jpg")',
          }}
        ><img style={{backgroundSize:'cover',width:'100%'}} src={thuNen.imgSrc_png}/></div>
      </div>
    )
}
