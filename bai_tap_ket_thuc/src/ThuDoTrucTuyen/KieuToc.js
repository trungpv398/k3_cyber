import React from "react";
import { useSelector,useDispatch } from "react-redux";
import { thuKieuTocAction } from "../redux/actions/ThuDoTrucTuyenAction";

export default function KieuToc() {
  const danhSachKieuToc = useSelector(
    (state) => state.ThuDoTrucTuyenReducer.KieuToc
  );
  const dispatch = useDispatch();

  const renderDanhSachKieuToc = () => {
    return danhSachKieuToc.map((item, index) => {
      return (
        <div className="col-md-3">
          <div className="card text-center">
            <img src={item.imgSrc_jpg} />
            <h4>
              <b>{item.name}</b>
            </h4>
            <button onClick={()=>{
              dispatch(thuKieuTocAction(item))
            }}>Thử đồ</button>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="tab-pane container fade" id="tabHairStyle">
      <div className="container">
        <div className="row">{renderDanhSachKieuToc()}</div>
      </div>
    </div>
  );
}
