import React,{useState} from "react";
import { useSelector,useDispatch } from "react-redux";
import { thuAoAction } from "../redux/actions/ThuDoTrucTuyenAction";
import {animated,useTransition,useSpring,useTrail} from 'react-spring'


export default function Ao() {
  let [status,setStatus] = useState(true);
  const danhSachAo = useSelector(
    (state) => state.ThuDoTrucTuyenReducer.Ao
  );
  const dispatch = useDispatch();

    const propsUseTransition = useTransition(danhSachAo,item=>item.id,{
      from: { transform: 'translate3d(0,-40px,0)' },
      enter: { transform: 'translate3d(0,0px,0)' },
      leave: { transform: 'translate3d(0,-40px,0)' },
      config:{duration:2000}
    });


    const propsUseSpring = useSpring({
      opacity:1,
      from:{opacity:0},
      config:{duration:2000}
    })

  const renderDanhSachAo = () => {
    return propsUseTransition.map(({item,props, index}) => {
      return (
        <animated.div style={props} key={index} className="col-md-3">
          <div className="card text-center">
            <img src={item.imgSrc_jpg} />
            <h4>
              <b>{item.name}</b>
            </h4>
            <animated.button style={propsUseSpring} onClick={()=>{
              
              dispatch(thuAoAction(item))
            }}>Thử đồ</animated.button>
          </div>
        </animated.div>
      );
    });
  };

  return (
    <div className="tab-pane container active" id="tabTopClothes">
      <div className="container">
        <div className="row">{renderDanhSachAo()}</div>
      </div>
    </div>
  );
}
