import React from "react";
import { useSelector,useDispatch } from "react-redux";
import { thuQuanAction } from "../redux/actions/ThuDoTrucTuyenAction";

export default function Quan() {
  const danhSachQuan = useSelector((state) => state.ThuDoTrucTuyenReducer.Quan);
  const dispatch = useDispatch();

  const renderDanhSachQuan = () => {
    return danhSachQuan.map((item, index) => {
      return (
        <div className="col-md-3">
          <div className="card text-center">
            <img src={item.imgSrc_jpg} />
            <h4>
              <b>{item.name}</b>
            </h4>
            <button onClick={()=>{
              dispatch(thuQuanAction(item))
            }}>Thử đồ</button>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="tab-pane container fade" id="tabBotClothes">
      <div className="container">
        <div className="row">{renderDanhSachQuan()}</div>
      </div>
    </div>
  );
}
