import React from "react";
import { useSelector,useDispatch } from "react-redux";
import { thuGiayAction } from "../redux/actions/ThuDoTrucTuyenAction";

export default function GiayDep() {
  const danhSachGiayDep = useSelector(
    (state) => state.ThuDoTrucTuyenReducer.Giay
  );
  const dispatch = useDispatch();

  const renderDanhSachGiayDep = () => {
    return danhSachGiayDep.map((item, index) => {
      return (
        <div className="col-md-3">
          <div className="card text-center">
            <img src={item.imgSrc_jpg} />
            <h4>
              <b>{item.name}</b>
            </h4>
            <button onClick={()=>{
              dispatch(thuGiayAction(item))
            }}>Thử đồ</button>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="tab-pane container fade" id="tabShoes">
      <div className="container">
        <div className="row">{renderDanhSachGiayDep()}</div>
      </div>
    </div>
  );
}
