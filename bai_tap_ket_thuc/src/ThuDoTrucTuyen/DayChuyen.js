import React from "react";
import { useSelector,useDispatch } from "react-redux";
import { thuDayChuyenAction } from "../redux/actions/ThuDoTrucTuyenAction";

export default function DayChuyen() {
  const danhSachDayChuyen = useSelector(
    (state) => state.ThuDoTrucTuyenReducer.DayChuyen
  );
  const dispatch = useDispatch();

  const renderDanhSachDayChuyen = () => {
    return danhSachDayChuyen.map((item, index) => {
      return (
        <div className="col-md-3">
          <div className="card text-center">
            <img src={item.imgSrc_jpg} />
            <h4>
              <b>{item.name}</b>
            </h4>
            <button onClick={()=>{
              dispatch(thuDayChuyenAction(item))
            }}>Thử đồ</button>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="tab-pane container fade" id="tabNecklaces">
      <div className="container">
        <div className="row">{renderDanhSachDayChuyen()}</div>
      </div>
    </div>
  );
}
