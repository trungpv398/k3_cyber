import React from "react";
import "./ThuDoTrucTuyen.css";
import Menu from "./Menu"
import Ao from "./Ao";
import Quan from "./Quan";
import GiayDep from "./GiayDep";
import TuiXach from "./TuiXach";
import DayChuyen from "./DayChuyen";
import KieuToc from "./KieuToc";
import Nen from "./Nen";

import ThuDo from "./ThuDo";

export default function ThuDoTrucTuyen() {
 
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-sm-12">
          <div className="card">
            <div className="text-center">
              <img src="./images/cybersoft.png" alt="Card image" />
            </div>
            <div className="card-body">
              <h4 className="card-title text-center">
                CyberLearn - Học lập trình trực tuyến - Dự án thử đồ trực tuyến
                - Virtual Dressing Room 
              </h4>
            </div>
          </div>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-md-8">
          <Menu/>
          <div className="well">
            {/* Tab panes */}
            <div className="tab-content">
            <Ao/>  
            <Quan/>
            <GiayDep/>
            <TuiXach/>
            <DayChuyen/>
            <KieuToc/>
            <Nen/>                          
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <ThuDo/>


        </div>
      </div>
    </div>
  );
}
