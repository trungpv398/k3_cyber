import React from 'react';
import { useSelector,useDispatch } from "react-redux";
import { thuAnhNen } from '../redux/actions/ThuDoTrucTuyenAction';

export default function Nen() {
  const danhSachNen = useSelector((state) => state.ThuDoTrucTuyenReducer.Nen);
  const dispatch = useDispatch();

  const renderDanhSachNen = () =>{
    return danhSachNen.map((item,index)=>{
        return (
          <div className="col-md-3">
          <div className="card text-center">
            <img src={item.imgSrc_jpg} />
            <h4>
              <b>{item.name}</b>
            </h4>
            <button onClick={()=>{
              dispatch(thuAnhNen(item))
            }}>Thử đồ</button>
          </div>
        </div>
        )
    });
  }
    return (
        <div className="tab-pane container fade" id="tabBackground">
        <div className="container">
          <div className="row">
            {renderDanhSachNen()}
          </div>
        </div>
      </div>
    )
}
