import React from 'react';
import { useSelector,useDispatch } from "react-redux";
import { thuTuiAction } from '../redux/actions/ThuDoTrucTuyenAction';

export default function TuiXach() {

    const danhSachTuiXach = useSelector((state)=>state.ThuDoTrucTuyenReducer.TuiXach);
    const dispatch = useDispatch(); 

    const renderDanhSachTuiXach = () =>{
        return danhSachTuiXach.map((item,index)=>{
            return (
                <div className="col-md-3">
                <div className="card text-center">
                  <img src={item.imgSrc_jpg} />
                  <h4>
                    <b>{item.name}</b>
                  </h4>
                  <button onClick={()=>{
                    dispatch(thuTuiAction(item))
                  }}>Thử đồ</button>
                </div>
              </div>
            )
        })
    }

    return (
        <div className="tab-pane container fade" id="tabHandBags">
                <div className="container">
                    <div className="row">
                        {renderDanhSachTuiXach()}
                    </div>
                </div>
              </div>
    )
}
