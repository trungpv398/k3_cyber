import { thu_ao, thu_giay, thu_quan, thu_tui,thu_day_chuyen, thu_kieu_toc, thu_anh_nen } from "../types/ThuDoTrucTuyenTypes"

export const thuAoAction = (item)=>{
    return{
        type: thu_ao,
        item
    }
}
export const thuQuanAction = (item)=>{
    return{
        type:thu_quan,
        item
    }
}
export const thuTuiAction = (item) =>{
    return{
        type:thu_tui,
        item
    }
}
export const thuGiayAction = (item)=>{
    return{
        type:thu_giay,
        item
    }
}
export const thuDayChuyenAction = (item)=>{
    return{
        type:thu_day_chuyen,
        item
    }
}
export const thuKieuTocAction = (item)=>{
    return{
        type:thu_kieu_toc,
        item
    }
}
export const thuAnhNen = (item) =>{
    return{
        type:thu_anh_nen,
        item
    }
}